class CreatePackageTrackings < ActiveRecord::Migration[5.1]
  def change
    create_table :package_trackings do |t|
      t.text :description
      t.references :package, foreign_key: true

      t.timestamps
    end
  end
end
