class CreateInvitedUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :invited_users do |t|
      t.string :email
      t.integer :role
      t.string :code
      t.boolean :status

      t.timestamps
    end
  end
end
