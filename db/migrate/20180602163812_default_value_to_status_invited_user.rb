class DefaultValueToStatusInvitedUser < ActiveRecord::Migration[5.1]
  def change
    change_column :invited_users, :status, :boolean, default: false
  end
end
