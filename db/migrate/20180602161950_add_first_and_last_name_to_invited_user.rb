class AddFirstAndLastNameToInvitedUser < ActiveRecord::Migration[5.1]
  def change
    add_column :invited_users, :first_name, :string
    add_column :invited_users, :last_name, :string
  end
end
