class CreatePackages < ActiveRecord::Migration[5.1]
  def change
    create_table :packages do |t|
      t.string :name
      t.text :description
      t.string :source
      t.string :destination
      t.string :tracking_number

      t.timestamps
    end
  end
end
