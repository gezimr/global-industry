class PackageTracking < ApplicationRecord
	belongs_to :package

	def self.track_package(tracking_number)
		package = nil
		package = Package.find_by(tracking_number: tracking_number) if tracking_number.present?
		package
	end
end
