class Package < ApplicationRecord
	has_many :package_trackings
	validates_presence_of :name, :source, :description

	before_save :generate_tracking_number

	private
	def generate_tracking_number
		tracking_number = SecureRandom.hex(12)
		package         = Package.find_by(tracking_number: tracking_number) if Package.all.present?
		generate_tracking_number if package.present?
		self.tracking_number = tracking_number
	end
end
