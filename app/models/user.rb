class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validate :can_create, on: :create
  before_create :validate_role
  after_create :update_status
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: { admin: 0, packager: 1, enduser: 2 }
  # perdorimi i carriervave gem per img uploads
  mount_uploader :avatar, AvatarUploader

  private
  def can_create
    invited_user = InvitedUser.find_by(email: email)
    unless invited_user.present?
      errors.add(:user, "Just invited users can create account")
    end
  end

  def validate_role
    invited_user = InvitedUser.find_by(email: email)
    self.role = invited_user.role
  end

  def update_status
    invited_user = InvitedUser.find_by(email: email)
    invited_user.status = true
    invited_user.save
  end
end
