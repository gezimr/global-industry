class InvitedUser < ApplicationRecord
	validates :email, uniqueness: true
	validates :code, uniqueness: true

	after_create :generate_code

	private
	def generate_code
		code         = SecureRandom.urlsafe_base64
		invited_user = InvitedUser.find_by(code: code) if InvitedUser.all.present?
		generate_code if invited_user.present?
		self.code = code
		self.save
	end
end
