class PackageTrackingsController < ApplicationController
	before_action :authenticate_user!
	before_action :admin_permissions, except: [:track_package]
	def create
		@package_tracking = PackageTracking.new(package_tracking_params)

		package = Package.find(params[:id])
		@package_tracking.package = package
		respond_to do |format|
			if @package_tracking.save
				format.html { redirect_to package_path(package), notice: 'Package Tracking was successfully created.' }
				format.json { render :show, status: :created, location: package }
			else
				format.html { render :new }
				format.json { render json: package.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
	end

	def track_package
		@package = PackageTracking.track_package(params[:search_param])

		@msg = "No packages found with this tracking number, please check your tracking number and search again." unless @package.present?
		@msg = "" if @package.present?
	end

	private
	def package_tracking_params
		params.require(:package_tracking).permit(:description, :package_id)
	end
end
