class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	before_action :configure_permitted_parameters, if: :devise_controller?
	layout :pick_layout

	def admin_permissions
		redirect_to root_path, notice: 'Permition Denied, you must to have admin permissions to do this action!' unless current_user.admin?
	end

	def packager_permissions
		redirect_to root_path, notice: 'Permition Denied, you must to have packager permissions to do this action!' unless current_user.packager? || current_user.admin?
	end

	protected

	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :role, :avatar, :avatar_cache, :remove_avatar])

		devise_parameter_sanitizer.permit(:account_update) do |user_params|
			user_params.permit(:first_name, :last_name, :current_password, :role, :password, :password_confirmation, :avatar, :avatar_cache, :remove_avatar)
		end
	end

	def pick_layout
		if user_signed_in?
			return 'application' if current_user.admin? || current_user.packager?
			return 'home' if current_user.enduser?
		end
		'login'
	end
end
