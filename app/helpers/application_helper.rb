module ApplicationHelper
	def active_class(path)
		# 'selected' if current_page?(path)
		current_page?(path) ? 'active' : ''
	end
	def is_active_tab(controller)
		controller_path == controller ? 'active' : nil
	end
end
