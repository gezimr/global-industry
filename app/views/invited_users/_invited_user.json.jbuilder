json.extract! invited_user, :id, :created_at, :updated_at
json.url invited_user_url(invited_user, format: :json)
