require 'test_helper'

class InvitedUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invited_user = invited_users(:one)
  end

  test "should get index" do
    get invited_users_url
    assert_response :success
  end

  test "should get new" do
    get new_invited_user_url
    assert_response :success
  end

  test "should create invited_user" do
    assert_difference('InvitedUser.count') do
      post invited_users_url, params: { invited_user: {  } }
    end

    assert_redirected_to invited_user_url(InvitedUser.last)
  end

  test "should show invited_user" do
    get invited_user_url(@invited_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_invited_user_url(@invited_user)
    assert_response :success
  end

  test "should update invited_user" do
    patch invited_user_url(@invited_user), params: { invited_user: {  } }
    assert_redirected_to invited_user_url(@invited_user)
  end

  test "should destroy invited_user" do
    assert_difference('InvitedUser.count', -1) do
      delete invited_user_url(@invited_user)
    end

    assert_redirected_to invited_users_url
  end
end
