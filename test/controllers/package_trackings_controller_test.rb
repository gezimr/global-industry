require 'test_helper'

class PackageTrackingsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get package_trackings_create_url
    assert_response :success
  end

  test "should get destroy" do
    get package_trackings_destroy_url
    assert_response :success
  end

end
