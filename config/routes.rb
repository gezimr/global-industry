Rails.application.routes.draw do
  get 'package_trackings/create'

  get 'package_trackings/destroy'

	devise_for :users, controllers: { sessions:      'users/sessions', passwords: 'users/passwords', confirmations: 'users/confirmations',
	                                  registrations: 'users/registrations' }
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	devise_scope :user do
		get '/users/sign_up/:code', to: 'users/registrations#new', as: :sign_up
		# match '/users/:id', to: 'users/registrations#delete_user', via: :delete, as: :delete_user
	end

	root 'package_trackings#track_package'
	resources :invited_users
	resources :packages
  post 'package_trackings/create/:id', to: 'package_trackings#create', as: :package_trackings
	get 'package_trackings/track_package', to: 'package_trackings#track_package', as: :track_package

end
